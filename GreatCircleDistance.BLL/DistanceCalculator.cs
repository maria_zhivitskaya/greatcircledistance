﻿using GreatCircleDistance.Model;
using System;

namespace GreatCircleDistance.BLL
{
    public class DistanceCalculator
    {
        private const double EarthRadius = 6_371_000d;

        public static double Distance(Point startPoint, Point endPoint)
        {
            if (startPoint == null) throw new ArgumentNullException(nameof(startPoint));
            if (endPoint == null) throw new ArgumentNullException(nameof(endPoint));

            return Distance(startPoint.Longitude, startPoint.Latitude, endPoint.Longitude, endPoint.Latitude);
        }

        public static double Distance(
        decimal startLongitude,
        decimal startLatitude,
        decimal endLongitude,
        decimal endLatitude)
        {
            if (!IsLatitudeValid(startLatitude)) throw new ArgumentException(nameof(startLatitude));
            if (!IsLatitudeValid(endLatitude)) throw new ArgumentException(nameof(endLatitude));
            if (!IsLongitudeValid(startLongitude)) throw new ArgumentException(nameof(startLongitude));
            if (!IsLongitudeValid(endLongitude)) throw new ArgumentException(nameof(endLongitude));

            var startLongitudeInRadians = ToRadians(startLongitude);
            var startLatitudeInRadians = ToRadians(startLatitude);
            var endLongitudeInRadians = ToRadians(endLongitude);
            var endLatitudeInRadians = ToRadians(endLatitude);

            return Math.Acos(Math.Sin(startLatitudeInRadians) * Math.Sin(endLatitudeInRadians)
                + Math.Cos(startLatitudeInRadians) * Math.Cos(endLatitudeInRadians)
                * Math.Cos(startLongitudeInRadians - endLongitudeInRadians))
                * EarthRadius;
        }

        private static double ToRadians(decimal degree)
        {
            return (double)degree * Math.PI / 180;
        }

        private static bool IsLatitudeValid(decimal latitude)
        {
            return latitude >= -90 && latitude <= 90;
        }

        private static bool IsLongitudeValid(decimal longitude)
        {
            return longitude >= -180 && longitude <= 180;
        }
    }
}
