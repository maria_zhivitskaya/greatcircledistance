﻿using GreatCircleDistance.BLL;
using GreatCircleDistance.DAL;
using GreatCircleDistance.Model;
using Microsoft.Extensions.Configuration;
using NLog;
using NLog.Extensions.Logging;
using System;
using System.IO;

namespace GreatCircleDistance.App
{
    public class GreatCircleDistanceApp
    {
        public IConfiguration Config { get; set; }
        public Point StartPoint => Config.GetSection("StartPoint").Get<Point>();
        public double Distance => Config.GetSection("Distance").Get<double>();
        public string InputPath => Config.GetSection("InputPath").Get<string>();
        public string OutputPath => Config.GetSection("OutputPath").Get<string>();

        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        public GreatCircleDistanceApp()
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(Path.Combine(AppContext.BaseDirectory))
               .AddJsonFile("appsettings.json", optional: true);
            Config = builder.Build();

            LogManager.Configuration = new NLogLoggingConfiguration(Config.GetSection("NLog"));
        }

        public void Run()
        {
            try
            {
                Logger.Info("Application started...");

                var customerProvider = new FileCustomerProvider(InputPath, OutputPath);
                var processor = new DistanceProcessor(customerProvider);

                Logger.Info($"Finding customers within {Distance} meters...");
                var customersInDistance = processor.FindCustomersInDistance(StartPoint, Distance);
                customerProvider.OutputCustomers(customersInDistance);
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex, ex.Message);
            }
            finally
            {
                Logger.Info("Application terminated. Press <enter> to exit...");
                Console.ReadLine();
            }
        }
    }
}
