﻿namespace GreatCircleDistance.Model
{
    public class Point
    {
        public Point() { }

        public Point(decimal latitude, decimal longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
