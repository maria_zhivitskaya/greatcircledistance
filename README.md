GREAT CIRCLE DISTANCE APPLICATION
-----------------------------

The application searches for customers (from *customers.txt* file) within a specified distance using **Great-circle distance** algorithm.

After building the application, there will be *appsettings.json* file in the *output* directory.
In this file, a *start point*, a *distance* (measured in meters) and an *input/output file names* are specified and can be configured.
An example of the application output could be found in *initialOutput.txt* file.

Before building the application, please make sure that **.net core 3.1** is installed.
You can download it [here](https://dotnet.microsoft.com/download/dotnet-core/3.1)

## Build and run the application
* Run build.cmd to build the application
* The 'output' directory will be created
* Run run.cmd to execute the application
* 'output.txt' file will be generated in the 'output' folder

## Run tests
* Run run-tests.cmd