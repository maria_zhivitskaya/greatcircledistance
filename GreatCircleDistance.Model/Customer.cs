﻿using Newtonsoft.Json;

namespace GreatCircleDistance.Model
{
    public class Customer
    {
        [JsonProperty("user_id")]
        public int UserId { get; set; }
        public string Name { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        public Point GetPoint()
        {
            return new Point(Latitude, Longitude);
        }
    }
}
