﻿using GreatCircleDistance.Model;
using System.Collections.Generic;

namespace GreatCircleDistance.DAL
{
    public interface ICustomerProvider
    {
        IEnumerable<Customer> GetCustomers();
    }
}
