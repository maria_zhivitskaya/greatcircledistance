﻿using GreatCircleDistance.Model;
using System.Collections.Generic;

namespace GreatCircleDistance.DAL
{
    interface ICustomerOutput
    {
        void OutputCustomers(IEnumerable<Customer> customers);
    }
}
