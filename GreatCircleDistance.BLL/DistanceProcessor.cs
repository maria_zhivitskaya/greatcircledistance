﻿using GreatCircleDistance.DAL;
using GreatCircleDistance.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GreatCircleDistance.BLL
{
    public class DistanceProcessor
    {
        private readonly ICustomerProvider _customerProvider;

        public DistanceProcessor(ICustomerProvider customerProvider)
        {
            _customerProvider = customerProvider;
        }

        public IEnumerable<Customer> FindCustomersInDistance(Point startPoint, double distance)
        {
            return _customerProvider
                .GetCustomers()
                .Where(_ => IsInDistance(startPoint, _.GetPoint(), distance));
        }

        private static bool IsInDistance(Point startPoint, Point endPoint, double distance)
        {
            if (distance < 0) throw new ArgumentException(nameof(distance));

            return DistanceCalculator.Distance(startPoint, endPoint) <= distance;
        }
    }
}
