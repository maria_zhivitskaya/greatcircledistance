using GreatCircleDistance.Model;
using System;
using Xunit;

namespace GreatCircleDistance.BLL.Test
{
    public class DistanceCalculatorTests
    {
        [Fact]
        public void Distance_SamePoint_ZeroDistance()
        {
            // Arrange
            decimal latitude = -10;
            decimal longitude = 23.7M;
            var startPoint = new Point(latitude, longitude);
            var endPoint = new Point(latitude, longitude);
            var expected = 0;

            // Act
            var actual = DistanceCalculator.Distance(startPoint, endPoint);

            // Assert
            Assert.Equal(expected, actual, 0);
        }

        [Theory]
        [InlineData(-200)]
        [InlineData(-180.1)]
        [InlineData(180.1)]
        [InlineData(200.5)]
        public void Distance_InvalidLongitude_ThrowsException(decimal longitude)
        {
            // Arrange
            decimal latitude = 1;
            var startPoint = new Point(latitude, longitude);
            var endPoint = new Point(latitude, longitude);

            // Act
            // Assert
            Assert.Throws<ArgumentException>(() => DistanceCalculator.Distance(startPoint, endPoint));
        }

        [Theory]
        [InlineData(-100)]
        [InlineData(-90.1)]
        [InlineData(90.1)]
        [InlineData(100.5)]
        public void Distance_InvalidLatitude_ThrowsException(decimal latitude)
        {
            // Arrange
            decimal longitude = 1;
            var startPoint = new Point(latitude, longitude);
            var endPoint = new Point(latitude, longitude);

            // Act
            // Assert
            Assert.Throws<ArgumentException>(() => DistanceCalculator.Distance(startPoint, endPoint));
        }

        [Fact]
        public void Distance_InvalidStartPoint_ThrowsException()
        {
            // Arrange
            Point startPoint = null;
            var endPoint = new Point(1, 1);

            // Act
            // Assert
            Assert.Throws<ArgumentNullException>(() => DistanceCalculator.Distance(startPoint, endPoint));
        }

        [Fact]
        public void Distance_InvalidEndPoint_ThrowsException()
        {
            // Arrange
            Point endPoint = null;
            var startPoint = new Point(1, 1);

            // Act
            // Assert
            Assert.Throws<ArgumentNullException>(() => DistanceCalculator.Distance(startPoint, endPoint));
        }

        [Theory]
        [InlineData(-6.043701, 52.986375, 41769)]
        [InlineData(-10.27699, 51.92893, 313256)]
        [InlineData(27, 53, 2196983)]
        public void Distance_ValidPoints_CalculateDistance(
        decimal endLongitude,
        decimal endLatitude,
        double expectedDistance)
        {
            // Arrange
            var startPoint = new Point(53.339428M ,- 6.257664M);
            var endPoint = new Point(endLatitude, endLongitude);

            // Act
            var actual = DistanceCalculator.Distance(startPoint, endPoint);

            // Assert
            Assert.Equal(expectedDistance, actual, 0);
        }
    }
}
