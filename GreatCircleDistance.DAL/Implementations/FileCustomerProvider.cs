﻿using GreatCircleDistance.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GreatCircleDistance.DAL
{
    public class FileCustomerProvider : ICustomerProvider, ICustomerOutput
    {
        private string _inputFilePath;
        private string _outputFilePath;

        public FileCustomerProvider(string inputFilePath, string outputFilePath)
        {
            _inputFilePath = inputFilePath;
            _outputFilePath = outputFilePath;
        }

        public IEnumerable<Customer> GetCustomers()
        {
            using (StreamReader reader = new StreamReader(_inputFilePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    yield return JsonConvert.DeserializeObject<Customer>(line);
                }
            }
        }

        public void OutputCustomers(IEnumerable<Customer> customers)
        {
            var sortedCustomers = customers.OrderBy(_ => _.UserId);
            using (StreamWriter writer = new StreamWriter(_outputFilePath))
            {
                foreach (var customer in sortedCustomers)
                {
                    writer.WriteLine($"name: {customer.Name}, user_id: {customer.UserId}");
                }
            }
        }
    }
}
