﻿using GreatCircleDistance.Model;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace GreatCircleDistance.BLL.Test
{
    public class DistanceProcessorTests
    {
        [Theory]
        [InlineData(-100)]
        [InlineData(-0.1)]
        public void FindCustomersInDistance_NegativeDistance_ThrowsException(double distance)
        {
            // Arrange
            var customerProvider = NSubstitute.Substitute.For<DAL.ICustomerProvider>();
            customerProvider.GetCustomers().Returns(new List<Customer>() { new Customer() });
            var processor = new DistanceProcessor(customerProvider);
            var startPoint = new Point(0, 0);

            // Act
            // Assert
            Assert.Throws<ArgumentException>(() => processor.FindCustomersInDistance(startPoint, distance).ToList());
        }

        [Theory]
        [InlineData(100000, -6.043701, 52.986375, true)]
        [InlineData(1000, -16.043701, 52.986375, false)]
        [InlineData(0, -16.043701, 52.986375, false)]
        public void FindCustomersInDistance_ValidDistance_ReturnsCustomersInDistance(
            double distance,
            decimal endLongitude,
            decimal endLatitude,
            bool isInDistance)
        {
            // Arrange
            var customer = new Customer() { Latitude = endLatitude, Longitude = endLongitude };
            var customerProvider = NSubstitute.Substitute.For<DAL.ICustomerProvider>();
            customerProvider.GetCustomers().Returns(new List<Customer>() { customer });
            var processor = new DistanceProcessor(customerProvider);
            var startPoint = new Point(53.339428M, -6.257664M);

            // Act
            var customersInDistance = processor.FindCustomersInDistance(startPoint, distance).ToList();

            // Assert
            if (isInDistance)
            {
                Assert.Single(customersInDistance);
                Assert.Contains<Customer>(customer, customersInDistance);
            }
            else
            {
                Assert.Empty(customersInDistance);
            }
        }
    }
}
